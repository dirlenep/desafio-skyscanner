var modalBody = document.getElementById("idBodyModal");

// Pegar a cidade de origem para pesquisar o ID
var origem = document.getElementById("idOrigem");
origem.onblur = function () {
	origem = document.getElementById("idOrigem").value;
	if (origem.trim() != "") {
		buscarSiglaOrigem()
	} else {
		$('.modal').modal('show');
	}
}

// Pegar a cidade de destino para pesquisar o ID
var destino = document.getElementById("idDestino");
destino.onblur = function () {
	destino = document.getElementById("idDestino").value;
	if (destino.trim() != "") {
		buscarSiglaDestino()
	} else {
		$('.modal').modal('show');
	}
}

// Pesquisando o ID da cidade de Origem
async function buscarSiglaOrigem() {
	try {
		let response = await fetch("https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/autosuggest/v1.0/BR/BRL/pt-BR/?query=" + origem, {
			"method": "GET",
			"headers": {
				"x-rapidapi-host": "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com",
				"x-rapidapi-key": "591d9c43e1msh92def5cfaf025a2p1eec64jsndaa0fedca8a9"
			}
		})
		let dados = await response.json();
		return dados.Places[0].PlaceId;
	} catch (err) {
		console.log(err);
	}
}

// Pesquisando o ID da cidade de Destino
async function buscarSiglaDestino() {
	try {
		let response = await fetch("https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/autosuggest/v1.0/BR/BRL/pt-BR/?query=" + destino, {
			"method": "GET",
			"headers": {
				"x-rapidapi-host": "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com",
				"x-rapidapi-key": "591d9c43e1msh92def5cfaf025a2p1eec64jsndaa0fedca8a9"
			}
		})
		let dados = await response.json();
		return dados.Places[0].PlaceId;
	} catch (err) {
		console.log(err);
	}
}

// Pegar a data da Ida 
var ida = document.getElementById("idIda");
ida.onblur = function () {
	ida = document.getElementById("idIda").value
	if (ida.trim() == "") {
		$('.modal').modal('show');
	}
}

// Pegar a data da Volta 
var volta = document.getElementById("idVolta");
volta.onblur = function () {
	volta = document.getElementById("idVolta").value
	if (volta.trim() == "") {
		$('.modal').modal('show');
	}
}

// Calculando o valor das passagens 
var pesquisarViagem = document.getElementById("idBtnPesquisar");

var carrierName = [];
var quotesDirect = [];
var quotesMinPrice = [];

pesquisarViagem.onclick = async function calcularPassagem() {
	// Alterando a tela de pesquisa para a tela de carregamento
	document.getElementById("idForm").classList.add("visually-hidden");
	document.getElementById("idCarregando").classList.remove("visually-hidden");
	var mostrarResultado = document.getElementById("idMostrarRes");

	// Buscando o custo da passagem
	try {
		let response = await fetch("https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browseroutes/v1.0/BR/BRL/pt-BR/" + await buscarSiglaOrigem() + "/" + await buscarSiglaDestino() + "/" + volta + "?inboundpartialdate=" + ida, {
			"method": "GET",
			"headers": {
				"x-rapidapi-host": "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com",
				"x-rapidapi-key": "591d9c43e1msh92def5cfaf025a2p1eec64jsndaa0fedca8a9"
			}
		})
		let dados = await response.json();
		console.log(dados);
		if (dados.Routes.length != 0) {
			document.getElementById("idCarregando").classList.add("visually-hidden");
			document.getElementById("idResultado").classList.remove("visually-hidden");
			for (let i = 0; i < dados.Carriers.length; i++) {
				carrierName.push(dados.Carriers[i].Name);
			}
			for (let i = 0; i < dados.Quotes.length; i++) {
				if (dados.Quotes[i].Direct == true) {
					quotesDirect.push("Sim")
				} else {
					quotesDirect.push("Não")
				}
				quotesMinPrice.push(dados.Quotes[i].MinPrice);
			}
			for (let i = 0; i < carrierName.length; i++) {
				mostrarResultado.insertAdjacentHTML("beforeend", "Empresa: " + carrierName[i] + "<br>");
				mostrarResultado.insertAdjacentHTML("beforeend", "Vôo Direto: " + quotesDirect[i] + "<br>");
				mostrarResultado.insertAdjacentHTML("beforeend", "Valor Mínimo: R$ " + quotesMinPrice[i] + "<br>" + "<br>");
			}
		} else {
			document.getElementById("idErro").classList.remove("visually-hidden");
			document.getElementById("idMsnErro").innerText = "Não existem rotas com esses parâmetros, tente novamente."
		}
	} catch (err) {
		console.error(err);
		document.getElementById("idCarregando").classList.add("visually-hidden");
		document.getElementById("idErro").classList.remove("visually-hidden");
	}
}

var btnRecarregar = document.getElementById("idBtnRecarregar");
btnRecarregar.onclick = function () {
	document.location.reload(true);
}